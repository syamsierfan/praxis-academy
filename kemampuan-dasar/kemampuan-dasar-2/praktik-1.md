ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip
$ cd rhymes

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes
$ git init
Initialized empty Git repository in D:/gigil nitip/rhymes/.git/

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ touch README.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git add README.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git commit -m 'First commit'
[master (root-commit) dea447c] First commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ echo 'This repo is a collection of my favorite nursery rhymes.' >> README.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.txt

no changes added to commit (use "git add" and/or "git commit -a")

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git diff
warning: LF will be replaced by CRLF in README.txt.
The file will have its original line endings in your working directory
diff --git a/README.txt b/README.txt
index e69de29..c83e022 100644
--- a/README.txt
+++ b/README.txt
@@ -0,0 +1 @@
+This repo is a collection of my favorite nursery rhymes.

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git add README.txt
warning: LF will be replaced by CRLF in README.txt.
The file will have its original line endings in your working directory

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git commit -m 'Added project overview to README.txt'
[master 02e8f9f] Added project overview to README.txt
 1 file changed, 1 insertion(+)

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git add all-around-the-mulberry-bush.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   all-around-the-mulberry-bush.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        hokey-pokey.txt
        jack-and-jill.txt
        old-mother-hubbard.txt
        twinkle-twinkle.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git commit -m 'Added all-around-the-mulberry.txt'
[master 2657929] Added all-around-the-mulberry.txt
 1 file changed, 19 insertions(+)
 create mode 100644 all-around-the-mulberry-bush.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git add jack-and-jill.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git commit -m 'Added jack-and-jill.txt'
[master f7f0272] Added jack-and-jill.txt
 1 file changed, 12 insertions(+)
 create mode 100644 jack-and-jill.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git add .

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git commit -m 'Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt'
[master b918014] Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt
 3 files changed, 55 insertions(+)
 create mode 100644 hokey-pokey.txt
 create mode 100644 old-mother-hubbard.txt
 create mode 100644 twinkle-twinkle.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git log
commit b91801445c5167d534ea56a8cda9bac0ca8436fc (HEAD -> master)
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 13:10:21 2021 +0700

    Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt

    Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt


commit f7f0272d68932bcd1260fd053017f378fea453d7
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 13:09:19 2021 +0700

    Added jack-and-jill.txt

commit 265792978b940660e471699b27ddea3ffdacbf5b
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 13:08:46 2021 +0700

    Added all-around-the-mulberry.txt

commit 02e8f9ff9df60725c94c5eb228f1dfd9066ac073
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 12:48:18 2021 +0700

    Added project overview to README.txt

commit dea447c7b6dd383d914b24a210dec59661770ea1
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 12:44:30 2021 +0700

    First commit

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git log --oneline
b918014 (HEAD -> master) Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt
f7f0272 Added jack-and-jill.txt
2657929 Added all-around-the-mulberry.txt
02e8f9f Added project overview to README.txt
dea447c First commit

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git log -p
commit b91801445c5167d534ea56a8cda9bac0ca8436fc (HEAD -> master)
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 13:10:21 2021 +0700

    Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt

diff --git a/hokey-pokey.txt b/hokey-pokey.txt
new file mode 100644
index 0000000..7ce79a4
--- /dev/null
+++ b/hokey-pokey.txt
@@ -0,0 +1,15 @@
+You put your right foot in,
+You put your right foot out;
+You put your right foot in,
+And you shake it all about.
+You do the Hokey-Pokey,
+And you turn yourself around.
+That's what it's all about!
+
+You put your left foot in...
+You put your right hand in...
+You put your right side in...
+You put your nose in...
+You put your tail in...
+You put your head in...
+You put your whole self in...
\ No newline at end of file
diff --git a/old-mother-hubbard.txt b/old-mother-hubbard.txt
new file mode 100644
index 0000000..9b7730a
--- /dev/null
+++ b/old-mother-hubbard.txt
@@ -0,0 +1,34 @@
+Old Mother Hubbard
+Went to the cupboard
+To fetch her poor dog a bone;
+But when she came there
+The cupboard was bare,
+And so the poor dog had none.
+She took a clean dish
+To get him some tripe;
+But when she came back
+He was smoking a pipe.
+She went to the grocer's
+To buy him some fruit;
+But when she came back
+He was playing the flute.
+
+She went to the baker's
+To buy him some bread;
+But when she came back
+The poor dog was dead.
+
+She went to the undertaker's
+To buy him a coffin;
+But when she came back
+The poor dog was laughing.
+
+She went to the hatter's
+To buy him a hat;
+But when she came back
+He was feeding the cat.
+
+The dame made a curtsey,
+The dog made a bow;
+The dame said, "Your servant."
+The dog said, "Bow wow!"
\ No newline at end of file
diff --git a/twinkle-twinkle.txt b/twinkle-twinkle.txt
new file mode 100644
index 0000000..ffa65e4
--- /dev/null
+++ b/twinkle-twinkle.txt
@@ -0,0 +1,6 @@
+Twinkle, twinkle, little star,
+How I wonder what you are.
+Up above the world so high,
+Like a diamond in the sky.
+Twinkle, twinkle, little star,
+How I wonder what you are.
\ No newline at end of file

commit f7f0272d68932bcd1260fd053017f378fea453d7
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 13:09:19 2021 +0700

    Added jack-and-jill.txt

diff --git a/jack-and-jill.txt b/jack-and-jill.txt
new file mode 100644
index 0000000..43c6820
--- /dev/null
+++ b/jack-and-jill.txt
@@ -0,0 +1,12 @@
+Jack and Jill
+Went up the hill
+To fetch a pail of water.
+Jack fell down
+And broke his crown
+And Jill came tumbling after.
+Up Jack got
+And home did trot
+As fast as he could caper
+Went to bed
+And plastered his head
+With vinegar and brown paper.
\ No newline at end of file

commit 265792978b940660e471699b27ddea3ffdacbf5b
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 13:08:46 2021 +0700

    Added all-around-the-mulberry.txt

diff --git a/all-around-the-mulberry-bush.txt b/all-around-the-mulberry-bush.txt
new file mode 100644
index 0000000..b77d989
--- /dev/null
+++ b/all-around-the-mulberry-bush.txt
@@ -0,0 +1,19 @@
+All around the mulberry bush
+The monkey chased the weasel.
+The monkey thought 'twas all in fun.
+Pop! goes the weasel.
+
+A penny for a spool of thread,
+A penny for a needle.
+That's the way the money goes.
+Pop! goes the weasel.
+
+Up and down the City Road,
+In and out of the Eagle,
+That's the way the money goes.
+Pop! goes the weasel.
+
+Half a pound of tuppenney rice,
+Half a pound of treacle,
+Mix it up and make it nice,
+Pop! goes the weasel.
+Pop! goes the weasel.
+Half a pound of treacle,
+Mix it up and make it nice,
+Pop! goes the weasel.

commit 02e8f9ff9df60725c94c5eb228f1dfd9066ac073
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 12:48:18 2021 +0700

    Added project overview to README.txt

diff --git a/README.txt b/README.txt
index e69de29..c83e022 100644
--- a/README.txt
+++ b/README.txt
@@ -0,0 +1 @@
+This repo is a collection of my favorite nursery rhymes.

commit dea447c7b6dd383d914b24a210dec59661770ea1
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 12:44:30 2021 +0700

    First commit

diff --git a/README.txt b/README.txt
new file mode 100644
index 0000000..e69de29
(END)

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git remote add origin https://github.com/z4ck987/rhymes.git

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git push -u origin master
Enumerating objects: 17, done.
Counting objects: 100% (17/17), done.
Delta compression using up to 2 threads
Compressing objects: 100% (14/14), done.
Writing objects: 100% (17/17), 2.21 KiB | 226.00 KiB/s, done.
Total 17 (delta 2), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (2/2), done.
remote: This repository moved. Please use the new location:
remote:   https://github.com/Z4ck987/rhymes.git
To https://github.com/z4ck987/rhymes.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git checkout -b hickory-dickory
Switched to a new branch 'hickory-dickory'

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git add hickory-dickory-dock.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git commit -m 'Added hickory-dickory-dock.txt'
[hickory-dickory a82665e] Added hickory-dickory-dock.txt
 1 file changed, 5 insertions(+)
 create mode 100644 hickory-dickory-dock.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git push origin hickory-dickory
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 2 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 348 bytes | 348.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote: Resolving deltas: 100% (1/1), completed with 1 local object.
remote: This repository moved. Please use the new location:
remote:   https://github.com/Z4ck987/rhymes.git
remote:
remote: Create a pull request for 'hickory-dickory' on GitHub by visiting:
remote:      https://github.com/Z4ck987/rhymes/pull/new/hickory-dickory
remote:
To https://github.com/z4ck987/rhymes.git
 * [new branch]      hickory-dickory -> hickory-dickory

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git remote rename origin z4ck987

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git remote add praxis https://github.com/z4ck987/rhymes.git

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git remote
praxis
z4ck987

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git remote -v
praxis  https://github.com/z4ck987/rhymes.git (fetch)
praxis  https://github.com/z4ck987/rhymes.git (push)
z4ck987 https://github.com/z4ck987/rhymes.git (fetch)
z4ck987 https://github.com/z4ck987/rhymes.git (push)
bash: /mingw64/bin/git: Device or resource busy

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git fetch praxis
From https://github.com/z4ck987/rhymes
 * [new branch]      hickory-dickory -> praxis/hickory-dickory
 * [new branch]      master          -> praxis/master

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git branch -a
* hickory-dickory
  master
  remotes/praxis/hickory-dickory
  remotes/praxis/master
  remotes/z4ck987/hickory-dickory
  remotes/z4ck987/master

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git diff master hickory-dickory
diff --git a/hickory-dickory-dock.txt b/hickory-dickory-dock.txt
new file mode 100644
index 0000000..4d623fe
--- /dev/null
+++ b/hickory-dickory-dock.txt
@@ -0,0 +1,5 @@
+Hickory, dickory, dock,
+The mouse ran up the clock.
+The clock struck one,
+The mouse ran down!
+Hickory, dickory, dock.
\ No newline at end of file

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git log -1 -p
commit a82665ef70eb858f5f1b1f6f280560ef5d24601c (HEAD -> hickory-dickory, z4ck987/hickory-dickory, praxis/hickory-dickory)
Author: syamsierfan <syamsierfan@gmail.com>
Date:   Wed Jan 20 14:01:11 2021 +0700

    Added hickory-dickory-dock.txt

diff --git a/hickory-dickory-dock.txt b/hickory-dickory-dock.txt
new file mode 100644
index 0000000..4d623fe
--- /dev/null
+++ b/hickory-dickory-dock.txt
@@ -0,0 +1,5 @@
+Hickory, dickory, dock,
+The mouse ran up the clock.
+The clock struck one,
+The mouse ran down!
+Hickory, dickory, dock.
\ No newline at end of file

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (hickory-dickory)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'z4ck987/master'.

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git merge hickory-dickory
Updating b918014..a82665e
Fast-forward
 hickory-dickory-dock.txt | 5 +++++
 1 file changed, 5 insertions(+)
 create mode 100644 hickory-dickory-dock.txt
bash: /mingw64/bin/git: Device or resource busy

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git branch -D hickory-dickory
Deleted branch hickory-dickory (was a82665e).

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git push z4ck987 master
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote: This repository moved. Please use the new location:
remote:   https://github.com/Z4ck987/rhymes.git
To https://github.com/z4ck987/rhymes.git
   b918014..a82665e  master -> master

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git remote
praxis
z4ck987

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git remote -v
praxis  https://github.com/z4ck987/rhymes.git (fetch)
praxis  https://github.com/z4ck987/rhymes.git (push)
z4ck987 https://github.com/z4ck987/rhymes.git (fetch)
z4ck987 https://github.com/z4ck987/rhymes.git (push)

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git remote update
Fetching z4ck987
Fetching praxis
From https://github.com/z4ck987/rhymes
   b918014..a82665e  master     -> praxis/master

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (master)
$ git checkout master
Already on 'master'
Your branch is up to date with 'z4ck987/master'.

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git status
On branch z4ck987-changes
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        jack-be-nimble.txt
        mother-goose.txt

nothing added to commit but untracked files present (use "git add" to track)

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git add jack-be-nimble.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git commit -m 'Added jack-be-nimble.txt'
[z4ck987-changes ea0ea90] Added jack-be-nimble.txt
 1 file changed, 29 insertions(+)
 create mode 100644 jack-be-nimble.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git add mother-goose.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git commit -m 'Added mother-goose.txt'
[z4ck987-changes baf8531] Added mother-goose.txt
 1 file changed, 20 insertions(+)
 create mode 100644 mother-goose.txt

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git log --oneline
baf8531 (HEAD -> z4ck987-changes) Added mother-goose.txt
ea0ea90 Added jack-be-nimble.txt
a82665e (z4ck987/master, z4ck987/hickory-dickory, praxis/master, praxis/hickory-dickory, master) Added hickory-dickory-dock.txt
b918014 Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt
f7f0272 Added jack-and-jill.txt
2657929 Added all-around-the-mulberry.txt
02e8f9f Added project overview to README.txt
dea447c First commit

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git rebase -i a82665e
pick ea0ea90 Added jack-be-nimble.txt
pick ea0ea90 Added jack-be-nimble.txt
pick baf8531 Added mother-goose.txt

# Rebase a82665e..baf8531 onto a82665e (2 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#

ACER@DESKTOP-13HV37J MINGW64 /d/gigil nitip/rhymes (z4ck987-changes)
$ git log --oneline
baf8531 (HEAD -> z4ck987-changes) Added mother-goose.txt
ea0ea90 Added jack-be-nimble.txt
a82665e (z4ck987/master, z4ck987/hickory-dickory, praxis/master, praxis/hickory-dickory, master) Added hickory-dickory-dock.txt
b918014 Added old-mother-hubbard.txt, twinkle-twinkle.txt, hokey-pokey.txt
f7f0272 Added jack-and-jill.txt
2657929 Added all-around-the-mulberry.txt
02e8f9f Added project overview to README.txt
dea447c First commit
